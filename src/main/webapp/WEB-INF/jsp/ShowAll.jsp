<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List"%>
<html>
<head>
<title>Employee Database Show</title>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>
<table>
<tr>
<th>Employee Id</th>
<th>Employee Name</th>
<th>Employee salary</th>
</tr>
<c:forEach items="${resultShow}" var="res">
   <tr><td><c:out value="${res.id}"/></td>
      <td><c:out value="${res.name}"/></td>
      <td><c:out value="${res.salary}"/></td></tr>
</c:forEach>
</table>
<form action="/employeeFinalName" id="back">
<button type="submit" form="back">Go Back</button>
</form>
</body>
</html>
