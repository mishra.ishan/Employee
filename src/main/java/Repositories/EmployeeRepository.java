package Repositories;

import Controller.EmployeeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by root on 8/26/16.
 */

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel, Long> {
    EmployeeModel findById(Integer id);
}
