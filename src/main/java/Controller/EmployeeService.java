package Controller;

import Repositories.EmployeeRepository;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedHashTreeMap;
import com.google.gson.internal.LinkedTreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 9/1/16.
 */

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public void returnDeviceId (String inputJson) {
        Gson gson = new Gson();
        try {
            Map<String, Object> map = gson.fromJson(inputJson, HashMap.class);
            String payuId = "", sessionId = "", retryAttemp = "", merchantKey = "";
            payuId = String.valueOf(map.get("payuId"));
            sessionId = String.valueOf(map.get("sessionId"));
            merchantKey = String.valueOf(map.get("merchantKey"));
            LinkedTreeMap<String, Object> messageMap = (LinkedTreeMap<String, Object>) map.get("message");
            String retryAttempt = String.valueOf(messageMap.get("retryAttempt"));
            String orderId = null;
//            String deviceId = getDeviceId(orderId);
//            EventDetailsForDevice eventDetailsForDevice = new EventDetailsForDevice();
//            eventDetailsForDevice.setDeviceId(deviceId);
//            eventDetailsForDevice.setMerchantKey(merchantKey);
//            eventDetailsForDevice.setOrderId(orderId);
//            eventDetailsForDevice.setPayuId(payuId);
//            eventDetailsForDevice.setRetryAttempt(Integer.parseInt(retryAttemp));
//            eventDetailsForDeviceRepository.save(eventDetailsForDevice);
        } catch (Exception ex) {
//            logger.error("Exception in returnDeviceId function: \n" + ex.getStackTrace());
        }
    }

    public void add(Double sal, String name) {
        EmployeeModel employee = new EmployeeModel();
        employee.setName(name);
        employee.setSalary(sal);
        employeeRepository.save(employee);
    }

    public void remove(Integer id) {
        EmployeeModel employee = employeeRepository.findById(id);
        if(employee != null) {
            employeeRepository.delete(employee);
        }
    }

    public List<EmployeeModel> getAll() {
        return employeeRepository.findAll();
    }
}