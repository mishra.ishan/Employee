package Controller;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by root on 8/26/16.
 */

@Entity
@Table(name="Employee")
public class EmployeeModel implements Serializable{
    @Id
    @GeneratedValue
    @Column(name="EmpId")
    private Integer id;

    @Column(name="EmpName")
    private String name;

    @Column(name="EmpSal")
    private Double Salary;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getSalary() {
        return Salary;
    }

    public void setSalary(Double salary) {
        Salary = salary;
    }
}
