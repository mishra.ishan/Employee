package Controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 *
 * @author ishan.mishra
 */

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(EmployeeController.class);

    @RequestMapping(value = "/add")
    public ModelAndView add(
            @RequestParam(value = "sal") Double sal,
            @RequestParam(value="name") String name) {
        log.info("In inputEmployee controller add");
        employeeService.add(sal, name);
        List<EmployeeModel> employeeList = employeeService.getAll();
        log.info("Employee List size: "+employeeList.size());
        return new ModelAndView("ShowAll", "resultShow", employeeList);
    }

    @RequestMapping(value = "/remove")
    public ModelAndView remove(
            @RequestParam(value = "id") Integer id) {
        log.info("In inputEmployee controller remove");
        employeeService.remove(id);
        List<EmployeeModel> employeeList = employeeService.getAll();
        log.info("Employee List size: "+employeeList.size());
        return new ModelAndView("ShowAll", "resultShow", employeeList);
    }
}
