A very basic maven spring webmvc project with jpa-hibernate support. No front-end. 
Requirements:
1. Java version 1.7
2. MySql.
3. Any build tool (I have tested on maven)
4. Servlet Container (I have used tomcat)

Create a databases called "employeeDB" in mysql. You will need to configure mysql username and password beforehand in spring-servlet.xml. You can configure the mysql settings in config file (provided as spring.properties).
Rest all dependencies of projects shall be downloaded when the project shall be built using pom. 

To build it using maven:
Open terminal. Go to the cloned folder. Update the config file as per your mysql in 'src/main/webapp/WEB-INF/spring.properties'. Type mvn clean install at the project root (the folder containing pom file). 
Copy the war file (ie target/employee.war) into your deployment folder (.../tomcat/webapps for tomcat). Start the tomcat server. 

You can also check the build and deployment logs in /usr/local/tomcat/employeeLogs.log (The path and name of file can be reconfigured in src/main/resources/log4j.properties).


To run it after build: go to (http://localhost:<port_of_your_deployment_environment>/employee

For eg, I have tested it on tomcat which by default runs on 8080:

http://localhost:8080/employee